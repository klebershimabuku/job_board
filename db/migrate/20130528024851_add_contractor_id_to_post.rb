class AddContractorIdToPost < ActiveRecord::Migration[4.2]
  def change
    add_column :posts, :contractor_id, :integer
  end
end
