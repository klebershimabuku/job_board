class UpdateAllPostsToActive < ActiveRecord::Migration[4.2]
  def self.up
    execute "UPDATE posts SET active = 't';"
  end

  def self.down
    execute "UPDATE posts SET active = 'f';"
  end
end
