class AddFieldsToContractor < ActiveRecord::Migration[4.2]
  def change
    add_column :contractors, :phone_number, :string
    add_column :contractors, :mail_address, :string
  end
end
