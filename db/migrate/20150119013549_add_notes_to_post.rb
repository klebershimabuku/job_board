class AddNotesToPost < ActiveRecord::Migration[4.2]
  def change
    add_column :posts, :notes, :text
  end
end
