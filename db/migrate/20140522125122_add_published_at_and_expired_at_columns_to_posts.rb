class AddPublishedAtAndExpiredAtColumnsToPosts < ActiveRecord::Migration[4.2]
  def change
    add_column :posts, :published_at, :datetime
    add_column :posts, :expired_at, :datetime
  end
end
