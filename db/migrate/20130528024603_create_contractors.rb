class CreateContractors < ActiveRecord::Migration[4.2]
  def change
    create_table :contractors do |t|
      t.string :name
      t.string :address
      t.text :info

      t.timestamps
    end
  end
end
