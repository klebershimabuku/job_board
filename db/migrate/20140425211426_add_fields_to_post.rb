class AddFieldsToPost < ActiveRecord::Migration[4.2]
  def change
    add_column :posts, :requirements, :text
    add_column :posts, :salary, :string
    add_column :posts, :work_load, :string
    add_column :posts, :shift, :string
    add_column :posts, :day_off, :string
  end
end
