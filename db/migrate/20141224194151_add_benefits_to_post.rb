class AddBenefitsToPost < ActiveRecord::Migration[4.2]
  def change
    add_column :posts, :benefits, :text
  end
end
