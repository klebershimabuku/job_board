class ApplicationController < ActionController::Base
  protect_from_forgery

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  before_action :expire_hsts

  private

  def record_not_found
    render plain: "404 Not Found", status: 404
  end

  def expire_hsts
    response.headers["Strict-Transport-Security"] = "max-age=0"
  end
end
