module Admin
  class SessionsController < Devise::SessionsController
    layout "session"

    def new
      self.resource = resource_class.new(sign_in_params)
      store_location_for(resource, params[:redirect_to])
      super
    end

    def after_sign_in_path_for(resource)
      sign_in_url = new_user_session_url
      if request.referer == sign_in_url
        admin_posts_url
      else
        stored_location_for(resource) || request.referer || root_url
      end
    end
  end
end
