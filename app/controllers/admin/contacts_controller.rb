module Admin
  class ContactsController < AdminController
    def show
      contractor = Contractor.where(name: params[:contractor_name]).first
      render json: contractor.contacts.map(&:name)
    end

    def new
      contractor = Contractor.find(params[:contractor_id])
      @contact = contractor.contacts.build
    end

    def create
      @contractor = Contractor.find(params[:contractor_id])
      @contractor.contacts.build(contact_params)

      if @contractor.save
        redirect_to([:admin, @contractor], notice: "Contato criado com sucesso")
      else
        flash[:error] = @contractor.errors
        render :new
      end
    end

    def edit
      @contractor = Contractor.find(params[:contractor_id])
      @contact = @contractor.contacts.where(id: params[:id]).first
    end

    def update
      @contractor = Contractor.find(params[:contractor_id])
      contact = @contractor.contacts.where(id: params[:id]).first

      if contact.update_attributes(contact_params)
        redirect_to([:admin, @contractor], notice: "Contato alterado com sucesso")
      else
        flash[:error] = @contractor.errors
        render :edit
      end
    end

    protected

    def contact_params
      params.require(:contact).
        permit(:name, :phone_number, :mail_address)
    end
  end
end
