module Admin
  class ContractorsController < AdminController
    respond_to :html, :json

    def index
      @contractors = Contractor.all.
        order(id: 'DESC').
        paginate(page: params[:page], per_page: results_per_page)

      respond_with(@contractors)
    end

    def new
      @contractor = Contractor.new
    end

    def create
      @contractor = Contractor.new(contractor_params)

      if @contractor.valid?
        @contractor.save
        redirect_to(admin_contractor_path(@contractor), notice: "Empresa criada com sucess")
      else
        flash[:error] = @contractor.errors
        render :new
      end
    end

    def edit
      @contractor = Contractor.find(params[:id])
    end

    def update
      @contractor = Contractor.find(params[:id])

      if @contractor.update_attributes(contractor_params)
        redirect_to(admin_contractor_path(@contractor), notice: "Empresa atualizada com sucess")
      else
        flash[:error] = @contractor.errors
        render :edit
      end
    end

    def show
      @contractor = Contractor.includes(:posts, :contacts).find(params[:id])
      @contractor_posts = @contractor.posts.order(id: 'DESC').
        paginate(page: params[:page], per_page: 50)
      @contacts = @contractor.contacts.order(id: 'DESC')
    end

    protected

    def contractor_params
      params.require(:contractor).permit(
        :name,
        :address,
        :info,
        :phone_number,
        :mail_address
      )
    end

    def results_per_page
      WillPaginate.per_page = 100
    end
  end
end
