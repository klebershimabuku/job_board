module Admin
  class PostsController < AdminController
    def index
      @posts = Post.all.includes(:contractor).order(id: 'desc').paginate(page: params[:page], per_page: results_per_page)
    end

    def show
      @post = Post.includes(:contractor).where(id: params[:id]).first!
    end

    def edit
      @post = Post.includes(:contractor).where(id: params[:id]).first
      @contractors = Contractor.all
    end

    def update
      @post = Post.where(id: params[:id]).first

      set_tag_list_to_post_params(@post)

      if @post.update_attributes(post_params)
        redirect_to(admin_post_url(@post), notice: "Anúncio salvo com sucesso!")
      else
        flash[:error] = "Não foi possível salvar o anúncio"
        render :edit
      end
    end

    def publish
      @post = Post.find(params[:id])
      @post.publish

      redirect_to(admin_post_url(@post), notice: "Anúncio publicado com sucesso!")
    end

    def archive
      @post = Post.find(params[:id])
      @post.archive

      redirect_to(admin_post_url(@post), notice: "Anúncio arquivado com sucesso!")
    end

    protected

    def results_per_page
      WillPaginate.per_page = 100
    end

    def post_params
      params.require(:post).permit(
        :title, :location, :description, :requirements, :benefits,
        :salary, :shift, :day_off, :notes, :contractor_id, :contact_id, :tag_list
      )
    end

    def set_tag_list_to_post_params(post)
      provinces = tag_service(post_params["location"]).call
      @post.tag_list.add(provinces)
    end

    def tag_service(location)
      TagService.new(location)
    end
  end
end
