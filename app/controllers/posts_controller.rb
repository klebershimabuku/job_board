class PostsController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  respond_to :html

  def home
  end

  def feeds
    @posts = Post.published

    respond_to do |format|
      format.html
      format.rss  { render layout: false }
      format.atom { redirect_to feeds_posts_path(format: :rss), status: :moved_permanently }
    end
  end

  def index
    @posts = Post.for_listing.
      paginate(page: params[:page], per_page: 10).decorate

    if stale?(@posts)
      respond_with(@posts)
    end
  end

  def tags
    @posts = Post.for_listing
      .tagged_with(params[:tag])
      .paginate(page: params[:page], per_page: 10)
      .includes([:contractor, :tags]).decorate

      render action: :index
  end

  def show
    post = Post.where(id: params[:id]).includes(:contractor).first!

    @post = PostDecorator.decorate(post)

    respond_with(@post)
  end

  def new
    @post = Post.new(active: false)

    respond_with(@post)
  end

  def create
    @post = Post.new(post_params)
    @post.notes = ContractorParser.call(post_params)

    if @post.save(context: :web)
      Mailer.notify(params[:post]).deliver_now
      render :success
    else
      render :new
    end
  end

  def success; end

  private

  def record_not_found
    respond_to do |format|
      format.html { render layout: 'application',
                    template: 'shared/404',
                    status: 404 }
      format.all { render nothing: true, status: 404 }
    end
  end

  def post_params
    params.require(:post).permit(:title, :location, :salary, :shift, :day_off,
                                :description, :requirements, :benefits, :work_load,
                                :contractor_person_name,
                                :contractor_company_name,
                                :contractor_address,
                                :contractor_mail_address,
                                :contractor_phone_number)
  end
end
