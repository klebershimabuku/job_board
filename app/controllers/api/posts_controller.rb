module Api
  class PostsController < ApplicationController
    def published
      posts = Post.published
      render json: posts
    end
  end
end
