class TagService
    PROVINCES = %w(aichi-ken akita-ken aomori-ken chiba-ken ehime-ken fukui-ken fukuoka-ken fukushima-ken gifu-ken gunma-ken hiroshima-ken hokkaido-ken hyogo-ken ibaraki-ken ishikawa-ken iwate-ken kagawa-ken kagoshima-ken kanagawa-ken kochi-ken kumamoto-ken kyoto-fu mie-ken miyagi-ken miyazaki-ken nagano-ken nagasaki-ken nara-ken niigata-ken oita-ken okayama-ken okinawa-ken osaka-fu saga-ken saitama-ken shiga-ken shimane-ken shizuoka-ken tochigi-ken tokushima-ken tokyo-to tottori-ken toyama-ken wakayama-ken yamagata-ken yamaguchi-ken yamanashi-ken)

  attr_reader :location
  attr_accessor :tags

  def initialize(location)
    @location = location.downcase
    @tags     = []
  end

  def call
    classify(location)
  end

  protected

  def classify(location)
    location_words = split_words

    location_words.each { |word| tags << word if PROVINCES.include?(word) }

    tags
  end

  def split_words
    @location.split(',').map(&:strip)
  end
end
