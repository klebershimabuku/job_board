class Post < ApplicationRecord
  belongs_to :contractor
  has_one :contactable
  has_one :contact, through: :contactable

  scope :all_for_api, -> {
    select(:id,
           :title,
           :location,
           'SUBSTR(description, 1,150) as description',
           :published_at,
           :created_at,
           :expired_at,
           :status)
    .order('id DESC')
  }

  scope :for_listing, -> { where("published_at IS NOT NULL").order("id DESC") }
  scope :published,   -> { where(status: 'published') }
  scope :expired,     -> { where(status: 'expired') }

  attr_accessor :contractor_company_name, :contractor_person_name, :contractor_phone_number, :contractor_mail_address, :contractor_address

  validates :title, :location, :description, :salary, :shift, :day_off, :work_load, presence: true

  validates :contractor, presence: true, on: :admin

  with_options({on: :web}) do |for_user|
    for_user.validates :contractor_person_name, :contractor_company_name, :contractor_address, :contractor_phone_number, presence: true

    for_user.validates :contractor_mail_address, presence: true, format: { with:  /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
  end

  enum status: [:pending, :published, :expired, :cancelled, :archived]

  acts_as_taggable

  def publish
    self.active = true
    self.published_at = Time.now
    self.published!
    self.save!(context: :admin)
  end

  def expire
    self.active = false
    self.expired_at = Time.now
    self.expired!
    self.save!(validate: false)
  end

  def archive
    self.archived!
    self.save!(context: :admin)
  end

  def relateds
    Posts::RelatedPosts.find(self)
  end
end
