class Contractor < ApplicationRecord
  validates :name, :address, :phone_number, :mail_address, presence: true

  has_many :posts
  has_many :contacts, dependent: :destroy

  def total_posts
    posts.count
  end

  def last_post_created
    posts.order(id: "DESC").last.try(:created_at)
  end
end
