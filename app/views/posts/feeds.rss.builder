xml.instruct! :xml, :version => '1.0', :encoding => "UTF-8"
xml.rss :version => '2.0' do
  xml.channel do
    xml.title 'ShigotodoDoko Feeds - A sua referência de empregos no Japão'
    xml.description "Empregos no Japão em #{Date.today.year}"
    xml.link posts_url

    for post in @posts
      xml.item do
        xml.title post.title
        xml.description simple_format(post.description)
        xml.pubDate post.created_at.to_s(:rfc822)
        xml.contact simple_format(post.contractor.info)
        xml.link post_url(post)
        xml.guid post_url(post)
      end
    end
  end
end
