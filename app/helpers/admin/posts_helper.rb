module Admin
  module PostsHelper
    def will_paginate(collection_or_options = nil, options = {})
      if collection_or_options.is_a? Hash
	options, collection_or_options = collection_or_options, nil
      end
      unless options[:renderer]
	options = options.merge :renderer => MyCustomLinkRenderer if options[:admin]
      end
      super *[collection_or_options, options].compact
    end

    def title(title)
      title.truncate(50)
    end

    def location(location)
      location.truncate(30)
    end

    def status(post)
      return content_tag(:span, post.status, class: "new badge blue", :"data-badge-caption"=> "") if post.pending?
      return content_tag(:span, post.status, class: "new badge green", :"data-badge-caption"=> "") if post.published?
      return content_tag(:span, post.status, class: "new badge red", :"data-badge-caption"=> "") if post.expired?
      return content_tag(:span, post.status, class: "new badge orange", :"data-badge-caption"=> "") if post.archived?
    end

    def contractor_name(post)
      no_contractor_message = "Anúncio sem empresa anunciante"

      return no_contractor_message if post.pending?

      if post.contractor
        link_to(post.contractor.name, admin_contractor_path(post.contractor))
      else
        no_contractor_message
      end
    end

    def contractor_list
      Contractor.all.collect { |contractor| [contractor.name, contractor.id] }
    end
  end
end
