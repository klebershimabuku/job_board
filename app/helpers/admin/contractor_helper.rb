module Admin
  module ContractorHelper
    def last_post_created(contractor)
      return "-" unless contractor.last_post_created.present?
      contractor.last_post_created.strftime("%d/%m/%Y")
    end
  end
end
