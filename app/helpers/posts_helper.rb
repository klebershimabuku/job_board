module PostsHelper
  include ActsAsTaggableOn::TagsHelper

  def ribbon(post)
    if post.expired?
      content_tag(:div, nil, class: "ribbon") do
        content_tag(:span, "Expirado", class: "expired")
      end
    elsif post.recent_published?
      content_tag(:div, nil, class: "ribbon") do
        content_tag(:span, "Novo!", class: "new")
      end
    end
  end

  def resume(description = post.description)
    description.truncate(150)
  end

  def visit(post)
    link_to(content_tag(:label, "ver anúncio"), post_path(post), class: "view")
  end
end
