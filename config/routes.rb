Rails.application.routes.draw do
  get '/admin' => redirect("/users/sign_in")
  devise_for :users, controllers: { sessions: 'admin/sessions' }

  namespace :api do
    get '/posts/published', to: 'posts#published'
  end

  namespace :admin do
    resources :posts do
      put :publish, on: :member
      put :archive, on: :member
    end

    resources :contractors do
      resources :contacts
    end
    get 'contacts/:contractor_name', to: 'contacts#show'
  end

  root :to => 'posts#index'

  resources :posts, only: [:index, :show, :new, :create] do
    collection do
      get  :feeds, defaults: { format: 'rss' }
      post :tweet
      get :home
    end
  end
  get 'posts/tags/:tag', to: 'posts#tags', as: 'posts_tag_filter'

  get '/obrigado-anuncio-enviado-com-sucesso' => 'posts#success', as: :post_success

  get '/feedback', to: 'home#feedback', as: :feedback
end
