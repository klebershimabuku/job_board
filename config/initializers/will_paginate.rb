options = {
  page_links: false,
  previous_label: 'Página anterior',
  next_label: 'Próxima página'
}

WillPaginate::ViewHelpers.pagination_options.merge!(options) if defined? Rails
