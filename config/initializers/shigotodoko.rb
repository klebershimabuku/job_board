SHIGOTODOKO_URL = {
  production: 'http://www.shigotodoko.com',
  development: 'http://localhost:3000',
  test: 'http://localhost:3000'
}

Settings = YAML.load(File.read(Rails.root + 'config' + 'settings.yml'))[Rails.env].with_indifferent_access
