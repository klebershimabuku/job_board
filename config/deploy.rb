# config valid only for Capistrano 3.1
lock '3.2.1'

set :stages, %w(production staging)
set :default_stage, "staging"

set :application, 'shigotodoko'

set :scm, :git
set :repo_url, "git@bitbucket.org:klebershimabuku/job_board.git"
set :branch, 'master'

# Default deploy_to directory is /var/www/my_app
set :deploy_to, "/var/www/wrong_staging_dir"

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

set :ssh_options, {:forward_agent => true }
# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml config/environments/production.rb}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

SSHKit.config.command_map[:rake] = "bundle exec rake"

namespace :deploy do

  desc 'Create symlinks'
  task :create_symlinks do
    on roles(:app), in: :sequence, wait: 5 do
      puts "------------------------------------------------------------"
      puts "-- creating symlink files for database.yml and production.rb"
      puts "------------------------------------------------------------"
      execute "rm #{release_path}/config/database.yml"
      execute "rm #{release_path}/config/environments/production.rb"
      execute "ln -s #{shared_path}/config/database.yml #{release_path}/config/database.yml"
      execute "ln -s #{shared_path}/config/environments/production.rb #{release_path}/config/environments/production.rb"
    end
  end

  after "deploy:symlink:linked_files", "deploy:create_symlinks"

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      execute :mkdir, '-p', "#{release_path}/tmp"
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart
end
