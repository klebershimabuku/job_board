require 'rails_helper'

describe Post do

  let(:valid_attributes) do
    build(:post).attributes
  end

  let(:post) { create(:post) }

  it { should belong_to :contractor }
  it { should have_one :contactable }
  it { should have_one(:contact).through(:contactable) }

  it { should respond_to :contractor_company_name }
  it { should respond_to :contractor_person_name }
  it { should respond_to :contractor_phone_number }
  it { should respond_to :contractor_mail_address }
  it { should respond_to :contractor_address }

  it { should validate_presence_of :title }
  it { should validate_presence_of :location }
  it { should validate_presence_of :description }
  it { should validate_presence_of :salary }
  it { should validate_presence_of :shift }
  it { should validate_presence_of :day_off }
  it { should validate_presence_of :work_load }

  context 'scopes' do
    let!(:post) { create(:post) }

    describe '#all_for_api' do
      subject { Post.all_for_api.first }

      its(:id)              { is_expected.to be_present }
      its(:title)           { is_expected.to be_present }
      its(:location)        { is_expected.to be_present }
      its(:description)     { is_expected.to be_present }
      its(:created_at)      { is_expected.to be_present }
      its(:status)          { is_expected.to be_present }

      context 'when expired' do
        before { post.expire }
        its(:expired_at) { is_expected.to be_present }
      end

      context 'when published' do
        before { post.publish }
        its(:published_at) { is_expected.to be_present }
      end
    end
  end

  context "validations for user (contractors)" do
    [:contractor_company_name, :contractor_person_name, :contractor_phone_number, :contractor_address].each do |field|
      it "fails validation with no #{field}" do
        post = Post.new(valid_attributes)
        post.save(context: :web)
        expect(post.errors[field].size).to eq 1
      end
   end
    it "fails validation with no contractor_mail_address" do
      post = Post.new(valid_attributes)
      post.save(context: :web)
      expect(post.errors[:contractor_mail_address].size).to eq 2
    end
  end

  context "on publishing" do

    let(:post) do
      Post.new(valid_attributes).tap do |p|
        p.contractor = build(:contractor)
      end
    end

    describe ".publish" do
      before { post.publish }
      it { expect(post.active?).to be_truthy }
      it { expect(post.published_at).to be_present }
    end
  end

  describe '.expire' do
    let(:post) do
      Post.new(valid_attributes).tap do |p|
        p.contractor = build(:contractor)
        p.publish
      end
    end

    before { post.expire }
    it { expect(post.active?).to be_falsey }
    it { expect(post.expired?).to be_truthy }
    it { expect(post.expired_at).to be_present }
  end

  describe ".status" do
    let(:post) { build(:post) }

    it "initial state is pending" do
      expect(post.status).to eq('pending')
    end
    it '.published' do
      post.published!
      expect(post.status).to eq('published')
      expect(post).to be_published
    end
    it '.expired!' do
      post.expired!
      expect(post.status).to eq('expired')
      expect(post).to be_expired
    end
    it '.cancelled!' do
      post.cancelled!
      expect(post.status).to eq('cancelled')
      expect(post).to be_cancelled
    end
    it '.archived!' do
      post.archived!
      expect(post.status).to eq('archived')
      expect(post).to be_archived
    end
  end
end
