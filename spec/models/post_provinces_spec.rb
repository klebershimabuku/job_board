require 'rails_helper'

describe PostProvinces do
  it { should belong_to(:post) }
  it { should belong_to(:province) }

  it { should respond_to(:post_id) }
  it { should respond_to(:province_id) }
end
