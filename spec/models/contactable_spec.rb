require 'rails_helper'

RSpec.describe Contactable, :type => :model do
  it { should belong_to(:contact) }
  it { should belong_to(:post) }
end
