require 'rails_helper'

RSpec.describe Contact, :type => :model do
  it { should belong_to(:contractor) }
  it { should have_many(:contactables) }
  it { should have_many(:posts).through(:contactables) }

  context 'attributes' do
    it { should respond_to(:name) }
    it { should respond_to(:phone_number) }
    it { should respond_to(:mail_address) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:phone_number) }
    it { should validate_presence_of(:mail_address) }
  end
end
