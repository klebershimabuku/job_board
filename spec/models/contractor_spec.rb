require 'rails_helper'

describe Contractor do
  it { should have_many :posts }
  it { should have_many :contacts }
  it { should validate_presence_of :name }
  it { should validate_presence_of :address }
  it { should validate_presence_of :phone_number }
  it { should validate_presence_of :mail_address }

  let(:contractor) { create(:contractor) }

  describe "#total_posts" do
    it "returns count" do
      posts = create_list(:post, 3, contractor: contractor)

      expect(contractor.total_posts).to eq(3)
    end
  end

  describe "#last_posts_created" do

    it "returns date when present" do
      posts = create_list(:post, 3, contractor: contractor)

      expect(contractor.last_post_created).to be_present
    end

    it "returns nil when date not present" do
      expect(contractor.last_post_created).to be_nil
    end
  end
end
