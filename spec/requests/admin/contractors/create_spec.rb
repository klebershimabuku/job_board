require "rails_helper"

RSpec.describe "Admin - create a new contractor", type: :request do
  before { sign_in(create(:admin)) }

  it "creates a new contractor with success" do
    valid_params = {
      contractor: {
        name: "ShigotoDoko",
        address: "Aichi-ken",
        phone_number: "090-6464-6027",
        mail_address: "admin@shigotodoko.com"
      }
    }

    post "/admin/contractors", params: valid_params

    expect(response).to be_redirect
    expect(Contractor.count).to eq(1)
    expect(Contractor.last.name).to eq("ShigotoDoko")
  end

  it "render errors" do
    invalid_params = {
      contractor: {
        address: "Aichi-ken",
        phone_number: "090-6464-6027",
        mail_address: "admin@shigotodoko.com"
      }
    }

    post "/admin/contractors", params: invalid_params

    expect(response).to be_successful
    expect(flash["error"].count).to eq(1)
    expect(flash["error"].messages[:name]).to include("não pode ficar em branco")
    expect(Contractor.count).to eq(0)
  end
end
