require "rails_helper"

RSpec.describe "Admin contractors listing", type: :request do
  before do
    @contractor1 = create(:contractor, name: "company A", address: "company@acompany.com")
    @contractor2 = create(:contractor, name: "company B", address: "company@bcompany.com")
    @post = create(:post, contractor: @contractor1)

    @admin = create(:admin)
    sign_in(@admin)
  end

  context "lists all contractors" do
    subject { get "/admin/contractors" }
    before { subject }

    it "contains contractor name" do
      expect(response.body).to include(@contractor1.name)
      expect(response.body).to include(@contractor2.name)
    end
    it "contains contractor address" do
      expect(response.body).to include(@contractor1.address)
      expect(response.body).to include(@contractor2.address)
    end
    it "contains contractor total posts" do
      expect(response.body).to include(@contractor1.posts.count.to_s)
    end
    it "contains contractor last post create date" do
      expect(response.body).to include(@contractor1.posts.last.created_at.strftime("%d/%m/%Y"))
    end
  end

  context "paginating" do
    before do
      allow_any_instance_of(Admin::ContractorsController).to receive(:results_per_page) { 3 }
      create_list(:contractor, 4)
    end

    it "has the next page link" do
      get "/admin/contractors"
      expect(response.body).to include("chevron_right")
    end

    it "has the previous page link" do
      get "/admin/contractors"
      expect(response.body).to include("chevron_left")
   end
  end
end
