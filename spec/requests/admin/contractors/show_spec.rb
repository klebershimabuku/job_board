require "rails_helper"

RSpec.describe "Admin contractor details", type: :request do
  before do
    @contractor = create(:contractor, name: "company a", address: "aichi-ken toyohashi-shi")
    @post = create(:post, contractor: @contractor)
  end

  context "when authenticated" do
    before do
      admin = create(:admin)
      sign_in(admin)
    end

    it "displays the contractor details page" do
      get "/admin/contractors/#{@contractor.id}"

      expect(response.body).to include(@contractor.name)
      expect(response.body).to include(@contractor.address)
    end

    it "displays the last 10 posts" do
      get "/admin/contractors/#{@contractor.id}"

      expect(response.body).to include(@post.title)
    end
  end
end
