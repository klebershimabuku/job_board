require "rails_helper"

RSpec.describe "Admin - new contractor", type: :request do
  it "renders new contractor page" do
    sign_in(create(:admin))
    get "/admin/contractors/new"

    expect(response).to be_ok
  end
end
