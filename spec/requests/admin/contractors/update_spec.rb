require "rails_helper"

RSpec.describe "Admin - edit a contractor", type: :request do
  before { sign_in(create(:admin)) }

  it "renders a edit page" do
    contractor = create(:contractor)

    get "/admin/contractors/#{contractor.id}/edit"

    expect(response).to be_successful
  end

  it "updates a contractor info with success" do
    contractor = create(:contractor)

    updated_params = {
      contractor: { name: "another contractor name" }
    }

    put "/admin/contractors/#{contractor.id}", params: updated_params

    expect(response).to be_redirect
    contractor.reload
    expect(contractor.name).to eq("another contractor name")
  end
end
