require "rails_helper"

RSpec.describe "Admin edit contact for contractor", type: :request do
  context "when authenticated" do
    before do
      admin = create(:admin)
      sign_in(admin)
    end

    it "updates with success" do
      contact    = build(:contact, name: "Akira")
      contractor = create(:contractor, contacts: [contact])

      contact_params = {
        contact: {
          name: "Shima"
        }
      }

      put "/admin/contractors/#{contractor.id}/contacts/#{contact.id}", params: contact_params

      expect(response).to be_redirect

      contractor.reload
      expect(contractor.contacts.first.name).to eq("Shima")
    end
  end
end

