require "rails_helper"

RSpec.describe "Admin create contact for contractor", type: :request do
  context "when authenticated" do
    before do
      admin = create(:admin)
      sign_in(admin)
    end

    it "creates with success" do
      contractor = create(:contractor)

      contact_params = {
        contact: {
          name: "Akira Toriyama",
          phone_number: "090-1020-3040",
          mail_address: "akira@toriyama.jp"
        }
      }

      expect(contractor.contacts.count).to eq(0)

      post "/admin/contractors/#{contractor.id}/contacts", params: contact_params

      expect(response).to be_redirect
      expect(contractor.contacts.count).to eq(1)
    end
  end
end
