require "rails_helper"

RSpec.describe "Admin, edit post", type: :request do
  before do
    @post = create(:post, notes: "some notes", status: "pending")
  end

  context "when authenticated" do
    before do
      @admin = create(:admin)
      sign_in(@admin)
    end

    it "displays the edit form" do
      get "/admin/posts/#{@post.id}/edit"

      expect(response).to be_successful

      expect(response.body).to include(@post.title)
      expect(response.body).to include(@post.location)
      expect(response.body).to include(@post.status)
      expect(response.body).to include(@post.description)
      expect(response.body).to include(@post.requirements)
      expect(response.body).to include(@post.benefits)
      expect(response.body).to include(@post.salary)
      expect(response.body).to include(@post.shift)
      expect(response.body).to include(@post.day_off)
      expect(response.body).to include(@post.work_load)
      expect(response.body).to include(@post.notes)
    end
  end

  context "when not authenticated" do
    it "redirects to login page" do
      get "/admin/posts/#{@post.id}/edit"
      expect(response.status).to eq(302)
      expect(response.headers["Location"]).to end_with("/users/sign_in")
    end
  end
end

