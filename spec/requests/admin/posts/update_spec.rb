require "rails_helper"

RSpec.describe "Admin, update an post", type: :request do
  before do
    @contractor = create(:contractor)

    @post = create(:post, notes: "some notes", status: "pending")

    @post.save
  end

  context "when authenticated" do
    before do
      @admin = create(:admin)
      sign_in(@admin)
    end

    it "updates post with success" do
      params = {
        post: {
          notes: "modified notes",
          contractor_id: @contractor.id,
          location: "Aichi-ken, Toyohashi-shi"
        }
      }

      put "/admin/posts/#{@post.id}", params: params
      expect(response.status).to eq(302)

      post = Post.find(@post.id)
      expect(post.notes).to eq("modified notes")
      expect(post.contractor).to eq(@contractor)
      expect(post.tag_list).to include("aichi-ken")
    end
  end

  context "when not authenticated" do
    it "redirects to login page" do
      put "/admin/posts/#{@post.id}"
      expect(response.status).to eq(302)
      expect(response.headers["Location"]).to end_with("/users/sign_in")
    end
  end

end
