require "rails_helper"

RSpec.describe "Admin post details", type: :request do
  before do
    @post = create(:post, notes: "some notes", status: "pending")
  end

  context "when authenticated" do
    before do
      @admin = create(:admin)
      sign_in(@admin)
    end

    it "displays post details page" do
      get "/admin/posts/#{@post.id}"

      expect(response.body).to include(@post.title)
      expect(response.body).to include(@post.location)
      expect(response.body).to include(@post.status)
      expect(response.body).to include(@post.description)
      expect(response.body).to include(@post.requirements)
      expect(response.body).to include(@post.benefits)
      expect(response.body).to include(@post.salary)
      expect(response.body).to include(@post.shift)
      expect(response.body).to include(@post.day_off)
      expect(response.body).to include(@post.notes)
    end

    context "and post is published" do
      before { @post.publish }

      it "displays the contractor name" do
        get "/admin/posts/#{@post.id}"
        expect(response.body).to include(@post.contractor.name)
      end
    end

    context "and post is NOT published" do
      it "does not display the contractor name" do
        get "/admin/posts/#{@post.id}"
        expect(response.body).to_not include(@post.contractor.name)
      end

      it "displays the publish button" do
        get "/admin/posts/#{@post.id}"
        expect(response.body).to include("Publicar")
      end

      it "displays the archive button" do
        get "/admin/posts/#{@post.id}"
        expect(response.body).to include("Arquivar")
      end
    end

    context "publish" do
      it "publishes with success" do
        put "/admin/posts/#{@post.id}/publish"
        expect(response.status).to eq(302)

        redirect_location = response.headers["Location"]
        get redirect_location

        expect(response.body).to include("Anúncio publicado com sucesso")
        expect(@post.reload.status).to eq("published")
        expect(@post.reload.published_at).to be_present
      end
    end

    context "archive" do
      it "archives with success" do
        put "/admin/posts/#{@post.id}/archive"
        expect(response.status).to eq(302)

        redirect_location = response.headers["Location"]
        get redirect_location

        expect(response.body).to include("Anúncio arquivado com sucesso")
        expect(@post.reload.status).to eq("archived")
        expect(@post.reload.published_at).to_not be_present
      end
    end
  end

  context "when not authenticated" do
    it "redirects back to login page" do
      get "/admin/posts/#{@post.id}"
      expect(response.status).to eq(302)
    end
  end
end
