require "rails_helper"

RSpec.describe "Admin post listing", type: :request do
  before do
    @post = create(:post, status: "published")
    @admin = create(:admin)

    sign_in(@admin)
  end

  def create_a_pending_post_with_no_contractor
    create(:post, contractor: nil, status: "pending")
  end

  def create_a_expired_post_with_contractor
    create(:post, status: "expired")
  end

  context "lists all posts" do
    subject { get "/admin/posts" }
    before  { subject }

    it "contains the title" do
      expect(response.body).to include(@post.title)
    end

    it "contains the location" do
      expect(response.body).to include(@post.location)
    end

    it "contains the contractor name" do
      expect(response.body).to include(@post.contractor.name)
    end

    it "contains the creation date" do
      translated_date = I18n.l(@post.created_at, format: "%d/%m/%Y")
      expect(response.body).to include(translated_date)
    end

    it "contains the status" do
      expect(response.body).to include(@post.status)
    end

    it "returns 'no contractor'" do
      create_a_pending_post_with_no_contractor

      get "/admin/posts"

      expect(response).to be_ok
      expect(response.body).to include("Anúncio sem empresa anunciante")
    end

    it "returns the contractor name when expired" do
      Post.delete_all

      create_a_expired_post_with_contractor

      get "/admin/posts"

      expect(response).to be_ok
      expect(response.body).to include(@post.contractor.name)
    end
  end

  context "paginating" do
    before do
      allow_any_instance_of(Admin::PostsController).to receive(:results_per_page) { 3 }
      create_list(:post, 4)
    end

    it "has the next page link" do
      get "/admin/posts"
      expect(response.body).to include("chevron_right")
    end

    it "has the previous page link" do
      get "/admin/posts"
      expect(response.body).to include("chevron_left")
    end

  end
end
