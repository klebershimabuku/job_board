require "rails_helper"

RSpec.describe "Admin, get contacts by contractor name", type: :request do
  context "when authenticated" do
    before do
      @contractor = create(:contractor, name: "shigotodoko")
      @contractor.contacts = [create(:contact)]
      @contractor.save

      @admin = create(:admin)
      sign_in(@admin)
    end

    it "returns the contractors in json format" do
      get "/admin/contacts/#{@contractor.name}", params: {}, headers: { format: :json }

      expect(response).to be_successful
      expect(response.body).to eq(["Fulano"].to_json)
    end
  end
end
