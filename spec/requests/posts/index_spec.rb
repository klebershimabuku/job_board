require 'rails_helper'

describe 'Index Page', type: :request do
  context "when there are no posts" do
    it "displays no posts message" do
      get "/posts"
      expect(response.body).to include("Nenhum anúncio publicado.")
    end
  end

  context "when there are some posts" do
    before { create_list(:post, 20, published_at: 2.days.ago, contractor: build(:contractor)) }

    it "displays the post summary" do
      get "/posts"
      expect(response.body).to include("Pachinko")
      expect(response.body).to include("Toyohashi")
    end

    context 'Pagination' do
      it 'display a link to the next page' do
        get "/posts"
        expect(response.body).to include('Próxima página')
      end
    end
  end
end
