require "rails_helper"

describe "new post page", type: :request do
  it "displays the page correctly with all fields" do
    get "/posts/new"

    expect(response.body).to include("Novo Anúncio")
    expect(response.body).to include("Descreva o trabalho a ser realizado")
  end

  context "sending a new post" do
    before { get "/posts/new" }

    context "with valid param" do
      let(:params) { build_form_params }

      it "displays success message" do
        post "/posts", params: params

        expect(response.status).to eq(200)
        expect(response).to render_template(:success)
      end
    end

    context "with invalid params" do
      let(:params) do
        valid_params = build_form_params
        valid_params[:post][:contractor_phone_number] = nil
        valid_params
      end

      it "displays error message" do
        post "/posts", params: params

        expect(response.status).to eq(200)
        expect(response).to render_template(:new)
        expect(response.body).to include("Por favor verifique as informações abaixo")
        expect(response).to_not render_template(:success)
      end
    end
  end

  def build_form_params
    {
      post: {
        :title => "job title",
        :location => "aichi-ken, toyohashi-shi",
        :salary => "900 ienes",
        :shift => "hirukin",
        :day_off => "weekends",
        :description => "description",
        :requirements => "requirements",
        :benefits => "benefits",
        :work_load => "08:00 ~ 19:00",
        :contractor_person_name => "john",
        :contractor_company_name => "abc company",
        :contractor_address => "first avenue",
        :contractor_mail_address => "company@mailinator.com",
        :contractor_phone_number => "81 0120 111222"
      }
    }
  end
end
