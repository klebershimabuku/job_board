require 'rails_helper'

describe 'Post details page', type: :request do

  let(:post) { create(:post, active: true, published_at: 2.days.ago, contractor: build(:contractor)) }

  it "shows all informations" do
    get "/posts/#{post.id}"

    expect(response.body).to include post.title
    expect(response.body).to include post.location
    expect(response.body).to include post.description
    expect(response.body).to include post.day_off
    expect(response.body).to include post.work_load
    expect(response.body).to include post.shift
    expect(response.body).to include post.salary
    expect(response.body).to include post.requirements
    expect(response.body).to include post.benefits
    expect(response.body).to include post.contractor.name
    expect(response.body).to include post.contractor.phone_number
    expect(response.body).to include post.contractor.mail_address
  end
end
