require "rails_helper"

describe "Admin authentication", type: :request do
  it "redirects to the authentication page" do
    get "/admin"
    expect(response.status).to eq(301)
    expect(response.headers["Location"]).to match("/users/sign_in")
  end

  it "renders authentication page" do
    get "/users/sign_in"
    expect(response.status).to eq(200)
    expect(response.body).to include("Log in")
  end

  context "with valid credentials" do
    let(:admin) { create(:admin) }

    before { sign_in(admin) }

    it "sign in with success" do
      get "/admin/posts"
      expect(response.body).to include("Admin")
    end
  end
end
