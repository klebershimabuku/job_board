require 'rails_helper'

describe Shigotodoko::Message do

  let(:valid_attr) do
    {
      :name    => 'Nome do fulano',
      :email   => 'nome@fulano.com',
      :message => 'mensagem whatever'
    }
  end

  context 'valid attributes' do
    it { expect(Shigotodoko::Message.new(valid_attr)).to be_valid }
  end

  context 'invalid attributes' do
    it 'when missing name' do
      Shigotodoko::Message.new(email: 'nome@fulano.com',
                               message: 'mnsagem whaever').should_not be_valid
    end

    it 'when missing email' do
      Shigotodoko::Message.new(
        name: 'Nome do fulano',
        message: 'mensagem whatever').should_not be_valid
    end

    it 'when missing message' do
      Shigotodoko::Message.new(
        name: 'Nome do fulano',
        email: 'nome@fulano.com').should_not be_valid
    end

    ['test', 'test@test', 'test.com', 'test@test.a'].each do |w|
      it "when invalid email #{w}" do
        Shigotodoko::Message.new(
          name: 'Nome do fulano',
          email: "#{w}",
          message: 'mensagem whatever').should_not be_valid
      end
    end
  end
end
