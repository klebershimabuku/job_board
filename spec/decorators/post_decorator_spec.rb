require 'rails_helper'

RSpec.describe PostDecorator, type: :decorator do
  let(:post) { build(:post, contact: contact) }

  subject { described_class.decorate(post) }

  context 'with contact' do
    let(:contact) do
      build(:contact,
            name: 'Yukihiro Matsumoto',
            mail_address: 'matz@ruby.jp',
            phone_number: '090-123-123')
    end

    it { expect(subject.contact_name).to  eq('Yukihiro Matsumoto') }
    it { expect(subject.contact_email).to eq('matz@ruby.jp') }
    it { expect(subject.contact_phone).to eq('090-123-123') }
    it { expect(subject.contact_phone_and_name).to eq('090-123-123 (Yukihiro Matsumoto)') }
  end

  context 'without contact' do
    let(:contact) { nil }

    it { expect(subject.contact_name).to  eq('Empreiteira X') }
    it { expect(subject.contact_email).to eq('mail@company.co.jp') }
    it { expect(subject.contact_phone).to eq('0120-0120') }
    it { expect(subject.contact_phone_and_name).to eq('0120-0120') }
  end
end
