require 'rails_helper'

describe Mailer do

  context "#notify" do
    let(:params) do
      {
        :title => "job title",
        :location => "aichi-ken, toyohashi-shi",
        :salary => "900 ienes",
        :shift => "hirukin",
        :segment => "line",
        :day_off => "weekends",
        :description => "description",
        :requirements => "requirements",
        :benefits => "benefits",
        :work_load => "08:00 ~ 19:00",
        :contractor_person_name => "john",
        :contractor_company_name => "abc company",
        :contractor_address => "first avenue",
        :contractor_mail_address => "company@mailinator.com",
        :contractor_phone_number => "81 0120 111222"
      }
    end

    let(:mail) { described_class.notify(params) }

    it "has the title" do
      expect(mail.body).to match(params[:title])
    end
    it "has the location" do
      expect(mail.body).to match(params[:location])
    end
    it "has the salary" do
      expect(mail.body).to match(params[:salary])
    end
    it "has the shift" do
      expect(mail.body).to match(params[:shift])
    end
    it "has the day_off" do
      expect(mail.body).to match(params[:day_off])
    end
    it "has the description" do
      expect(mail.body).to match(params[:description])
    end
    it "has the requirements" do
      expect(mail.body).to match(params[:requirements])
    end
    it "has the benefits" do
      expect(mail.body).to match(params[:benefits])
    end
    it "has the work_load" do
      expect(mail.body).to match(params[:work_load])
    end
    it "has the contractor person name" do
      expect(mail.body).to match(params[:contractor_person_name])
    end
    it "has the company name" do
      expect(mail.body).to match(params[:contractor_company_name])
    end
    it "has the company address" do
      expect(mail.body).to match(params[:contractor_address])
    end
    it "has the company mail address" do
      expect(mail.body).to match(params[:contractor_mail_address])
    end
    it "has the company phone number" do
      expect(mail.body).to match(params[:contractor_phone_number])
    end
  end
end
