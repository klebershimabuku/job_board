require "rails_helper"

RSpec.describe PostsHelper, type: :helper do
  let(:post) { PostDecorator.decorate(build(:post)) }

  describe "#ribbon" do
    subject { helper.ribbon(post) }

    context "when expired" do
      before { post.expire }
      it { expect(subject).to include("Expirado") }
    end

    context "when recent published" do
      before { post.publish }
      it { expect(subject).to include("Novo!") }
    end
  end

  describe "#resume" do
    let(:long_resume) { (("A" * 150) + "X" * 5) }

    subject { helper.resume(long_resume) }

    it { expect(subject).to_not include("XXXXX") }
  end

  describe "#visit" do
    before  { post.save }

    subject { helper.visit(post) }

    it { expect(subject).to include("ver anúncio") }
  end
end
