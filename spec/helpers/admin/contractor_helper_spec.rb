require "rails_helper"

RSpec.describe Admin::ContractorHelper, type: :helper do
  let(:contractor) { create(:contractor) }

  describe "#last_post_created" do
    subject { helper.last_post_created(contractor) }

    it "returns formatted date when last post exist" do
      p = create(:post, contractor: contractor)
      expect(subject).to eq(p.created_at.strftime("%d/%m/%Y"))
    end

    it "returns a dash string when last posts not exist" do
      expect(subject).to eq("-")
    end
  end
end
