FactoryBot.define do
  factory :post do
    title 'Pachinko'
    location 'Aichi-ken, Toyohashi-shi'
    description 'Emprego fixo, salario compativel com o mercado'
    association :contractor, factory: :contractor
    requirements 'falar nihongo'
    benefits '2 horas extras diarias'
    salary '1000 ienes/hora'
    work_load 'das 8 as 20h'
    shift 'hirukin'
    day_off 'finais de semana'
  end
end
