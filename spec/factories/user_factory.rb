FactoryBot.define do
  factory :user do
    sequence(:email) { |email| "#{email}@shigotodoko.com" }
    password 'anypassword'
    password_confirmation 'anypassword'
  end

  factory :admin, class: 'User' do
    email 'admin@gmail.com'
    password 'anypassword'
    password_confirmation 'anypassword'
  end
end
