FactoryBot.define do
  factory :contact do
    contractor_id 123
    name "Fulano"
    phone_number "99102030"
    mail_address "fulano@shigotodoko.com"
  end
end
