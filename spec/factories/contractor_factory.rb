FactoryBot.define do
  factory :contractor do
    name 'Empreiteira X'
    address 'Tokyo-to, Tokyo-shi, 1-2-3'
    info 'job@empreiteira.com'
    phone_number '0120-0120'
    mail_address 'mail@company.co.jp'
  end
end
