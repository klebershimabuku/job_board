[![wercker status](https://app.wercker.com/status/fcc54a05085523220ddd72424e940df9/m "wercker status")](https://app.wercker.com/project/bykey/fcc54a05085523220ddd72424e940df9)

job_board
=========
A very simple job board rails application with Admin.

How to build the docker image
=========
$ docker build -t jobboard .

Now, you can run the `/bin/jb` commands.
